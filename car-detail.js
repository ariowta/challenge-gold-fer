
let detailMobil = JSON.parse(localStorage.getItem("mobilPilihan"));

let dataContainerArray = document.getElementsByClassName("filter-data");


// Menampilkan informasi filter
for (let i = 0; i < dataContainerArray.length; i++) {
    let dataContainer = dataContainerArray[i];

    if (i === 0) {
        dataContainer.innerHTML = `
            <p>${detailMobil.name}</p>
        `
    } else if (i === 1) {
        dataContainer.innerHTML = `
            <p>${detailMobil.category}</p>
            <img src = "./assets/fi_calendar.png" />
        `
    } else if (i === 2) {
        if (detailMobil.price >= 400000 && detailMobil.price <= 600000) {
            dataContainer.innerHTML = `
                <p>Rp 400.000 - Rp 600.000</p>
                <img src = "./assets/fi_clock.png" />
            `
        } else if (detailMobil.price < 400000) {
            dataContainer.innerHTML = `
                <p>< Rp 400.000</p>
                <img src = "./assets/fi_clock.png" />
            `
        } else if (detailMobil.price > 600000) {
            dataContainer.innerHTML = `
                <p>> Rp 600.000</p>
                <img src = "./assets/fi_clock.png" />
            `
        }
    } else if (i === 3) {
        if (detailMobil.status.toString() === "false") {
            dataContainer.innerHTML = `
                <p>Disewakan</p>
                <img src = "./assets/fi_users.png" />
            `
        } else if (detailMobil.status.toString() === "false") {
            dataContainer.innerHTML = `
                <p>Tidak Disewakan</p>
                <img src = "./assets/fi_users.png" />
            `
        }
    }
    
}
// Menampilkan informasi filter END 

// Menampilkan informasi harga

let detailHarga = document.querySelector(".detail-info__harga")
let priceConverter = Intl.NumberFormat('ID');

detailHarga.innerHTML = `
    <img src = ${detailMobil.image} style = "width:270px; height:auto"/>
    <div>
        <h4>${detailMobil.name}</h4>
        <div class="kategori-detail" style = "display:flex; gap:8px">
            <img src = "./assets/fi_users.png" />
            <p>${detailMobil.category}</p>
        </div>
    </div>
    <div class="kategori-detail" style = "display:flex; justify-content:space-between">
        <p style = "font-weight: bold">Total</p>
        <p style = "font-weight: bold">Rp ${priceConverter.format(detailMobil.price)}</p>
    </div>

`
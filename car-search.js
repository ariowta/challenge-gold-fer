const URL = "https://bootcamp-rent-car.herokuapp.com/admin/car";

const filter = document.getElementById("filter");


filter.addEventListener("click", getData);

async function getData() {

    let arrayFilter = [];
    
    let tipe = document.getElementById("tipe").value.trim();
    arrayFilter.push(tipe);

    let kategori = document.getElementById("kategori").value;
    arrayFilter.push(kategori);

    let harga = document.getElementById("harga").value;
    arrayFilter.push(harga);

    let status = document.getElementById("status").value;
    arrayFilter.push(status);

    console.log(arrayFilter)

    let loadingContainer = document.getElementById("loading");

    let result;

    try {
        loadingContainer.innerHTML = `<div>loading...</div>`
        const request = await fetch(URL);
        const response = await request.json();
        // const hasilFilter = await response.filter((data => data.id > 5));

        if (tipe.length == 0 && kategori.length == 0 && harga.length == 0 && status.length == 0) { // semua tidak diisi
            result = response;

            
            
        } else if (tipe.length > 0 && kategori.length > 0 && harga.length > 0 && status.length > 0) { // semua diisi
            // const data = getData()
            if (harga.includes("-")) {
                let hargaArray = harga.split('-').map(data => parseInt(data));
                let hargaMin = hargaArray[0];
                let hargaMax = hargaArray[1];
                
                result = response.filter(datum => datum.name == tipe && datum.category == kategori && datum.price >= hargaMin && datum.price <= hargaMax && datum.status.toString() == status);
                
                
            } else {
                let hargaInt = parseInt(harga);
                if (hargaInt === 400000) {
                    result = response.filter(datum => datum.name == tipe && datum.category == kategori && datum.price < hargaInt && datum.status.toString() == status);
                    
                } else if (hargaInt === 600000) {
                    result = response.filter(datum => datum.name == tipe && datum.category == kategori && datum.price > hargaInt && datum.status.toString() == status);
                    
                }
            }
            
        } else if (kategori.length > 0 && harga.length > 0 && status.length > 0) { //data 1 tidak diisi
            if (harga.includes("-")) {
                let hargaArray = harga.split('-').map(data => parseInt(data));
                let hargaMin = hargaArray[0];
                let hargaMax = hargaArray[1];
                
                result = response.filter(datum => datum.category == kategori && datum.price >= hargaMin && datum.price <= hargaMax && datum.status.toString() == status);
                
                
            } else {
                let hargaInt = parseInt(harga);
                if (hargaInt === 400000) {
                    result = response.filter(datum => datum.category == kategori && datum.price < hargaInt && datum.status.toString() == status);
                    
                } else if (hargaInt === 600000) {
                    result = response.filter(datum => datum.category == kategori && datum.price > hargaInt && datum.status.toString() == status);
                    
                }
            }

        } else if (tipe.length > 0 && kategori.length > 0 && harga.length > 0) { //data 4 tidak diisi
            if (harga.includes("-")) {
                let hargaArray = harga.split('-').map(data => parseInt(data));
                let hargaMin = hargaArray[0];
                let hargaMax = hargaArray[1];
                
                result = response.filter(datum => datum.name == tipe && datum.category == kategori && datum.price >= hargaMin && datum.price <= hargaMax);
                
                
            } else {
                let hargaInt = parseInt(harga);
                if (hargaInt === 400000) {
                    result = response.filter(datum => datum.name == tipe && datum.category == kategori && datum.price < hargaInt);
                    
                } else if (hargaInt === 600000) {
                    result = response.filter(datum => datum.name == tipe && datum.category == kategori && datum.price > hargaInt);
                    
                }
            }

        } else if (tipe.length > 0 && harga.length > 0 && status.length > 0) { //data 2 tidak diisi
            if (harga.includes("-")) {
                let hargaArray = harga.split('-').map(data => parseInt(data));
                let hargaMin = hargaArray[0];
                let hargaMax = hargaArray[1];
                
                result = response.filter(datum => datum.name == tipe && datum.price >= hargaMin && datum.price <= hargaMax && datum.status.toString() == status);
                
                
            } else {
                let hargaInt = parseInt(harga);
                if (hargaInt === 400000) {
                    result = response.filter(datum => datum.name == tipe && datum.price < hargaInt && datum.status.toString() == status);
                    
                } else if (hargaInt === 600000) {
                    result = response.filter(datum => datum.name == tipe && datum.price > hargaInt && datum.status.toString() == status);
                    
                }
            }

        } else if (tipe.length > 0 && kategori.length > 0 && status.length > 0) { //data 3 tidak diisi
            result = response.filter(datum => datum.name == tipe && datum.category == kategori && datum.status.toString() == status);
            
            

        } else if (harga.length > 0 && status.length > 0) { //data 1 dan 2 tidak diisi
            if (harga.includes("-")) {
                let hargaArray = harga.split('-').map(data => parseInt(data));
                let hargaMin = hargaArray[0];
                let hargaMax = hargaArray[1];
                
                result = response.filter(datum => datum.price >= hargaMin && datum.price <= hargaMax && datum.status.toString() == status);
                
                
            } else {
                let hargaInt = parseInt(harga);
                if (hargaInt === 400000) {
                    result = response.filter(datum => datum.price < hargaInt && datum.status.toString() == status);
                    
                } else if (hargaInt === 600000) {
                    result = response.filter(datum => datum.price > hargaInt && datum.status.toString() == status);
                    
                }
            } 
        } else if (tipe.length > 0 && status.length > 0) { //data 2 dan 3 tidak diisi
            result = response.filter(datum => datum.name == tipe && datum.status.toString() == status);
           
        } else if (tipe.length > 0 && kategori.length > 0) { //data 3 dan 4 tidak diisi
            result = response.filter(datum => datum.name == tipe && datum.category == kategori);
            
        } else if (kategori.length > 0 && status.length > 0) { //data 1 dan 3 tidak diisi
            result = response.filter(datum => datum.category == kategori && datum.status.toString() == status);
            
        } else if (tipe.length > 0 && harga.length > 0) { //data 2 dan 4 tidak diisi
            if (harga.includes("-")) {
                let hargaArray = harga.split('-').map(data => parseInt(data));
                let hargaMin = hargaArray[0];
                let hargaMax = hargaArray[1];
                
                result = response.filter(datum => datum.name == tipe  && datum.price >= hargaMin && datum.price <= hargaMax);
                
                
            } else {
                let hargaInt = parseInt(harga);
                if (hargaInt === 400000) {
                    result = response.filter(datum => datum.name == tipe && datum.price < hargaInt);
                    
                } else if (hargaInt === 600000) {
                    result = response.filter(datum => datum.name == tipe && datum.price > hargaInt);
                    
                }
            }
            
        } else if (kategori.length > 0 && harga.length > 0) { //data 1 dan 4 tidak diisi
            if (harga.includes("-")) {
                let hargaArray = harga.split('-').map(data => parseInt(data));
                let hargaMin = hargaArray[0];
                let hargaMax = hargaArray[1];
                
                result = response.filter(datum => datum.category == kategori  && datum.price >= hargaMin && datum.price <= hargaMax);
                
                
            } else {
                let hargaInt = parseInt(harga);
                if (hargaInt === 400000) {
                    result = response.filter(datum => datum.category == kategori && datum.price < hargaInt);
                    
                } else if (hargaInt === 600000) {
                    result = response.filter(datum => datum.category == kategori && datum.price > hargaInt);
                    
                }
            }
            
        } else if (status.length > 0) { //data 1,2 dan 3 tidak diisi or hanya 4
            result = response.filter(datum => datum.status.toString() == status);
            
        } else if (harga.length > 0) { //hanya 3
            if (harga.includes("-")) {
                let hargaArray = harga.split('-').map(data => parseInt(data));
                let hargaMin = hargaArray[0];
                let hargaMax = hargaArray[1];
                
                result = response.filter(datum => datum.price >= hargaMin && datum.price <= hargaMax);
                
                
            } else {
                let hargaInt = parseInt(harga);
                if (hargaInt === 400000) {
                    result = response.filter(datum => datum.price < hargaInt);
                    
                } else if (hargaInt === 600000) {
                    result = response.filter(datum => datum.price > hargaInt);
                    
                }
            } 
        } else if (kategori.length > 0) { //hanya 2
            result = response.filter(datum => datum.category == kategori);
            

        } else if (tipe.length > 0) { //hanya 1
            result = response.filter(datum => datum.name == tipe);
            
        }

        localStorage.setItem('hasilFilter', JSON.stringify(result));
        localStorage.setItem('dataFilter', JSON.stringify(arrayFilter));
        location.href = "filter-result.html"
        
        
    } catch (error) {
        loadingContainer.innerHTML = `
            <div>Data tidak ditemukan</div>
        `
    }
}


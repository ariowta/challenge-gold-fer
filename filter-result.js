let arrayInfoFilter = JSON.parse(localStorage.getItem("dataFilter"));
let arrayMobil = JSON.parse(localStorage.getItem("hasilFilter"));

let dataContainerArray = document.getElementsByClassName("filter-data");

// Menampilkan informasi filter
for (let i = 0; i < dataContainerArray.length; i++) {
    let dataInput = arrayInfoFilter[i];
    let dataContainer = dataContainerArray[i];

    if (dataInput === "false") {
        dataContainer.innerHTML = `
            <p>Disewakan</p>
        `
    } else if (dataInput === "true") {
        dataContainer.innerHTML = `
            <p>Tidak Disewakan</p>
        `
    } else if (i == 2 && dataInput.length > 0   ) {
        let priceConverter = Intl.NumberFormat('ID');
        if (dataInput.includes("-")) {
            let tarifSplit = dataInput.split("-");

            let convertedTarifArr =  tarifSplit.map(item => `Rp ${priceConverter.format(item)}`);
            dataInput = convertedTarifArr.join(" - ");
            
            dataContainer.innerHTML = `
            <p>${dataInput}</p>
        `
        } else if (dataInput === "400000") {
            dataContainer.innerHTML = `
            <p>< Rp ${priceConverter.format(dataInput)}</p>
        `
        } else if (dataInput === "600000") {
            dataContainer.innerHTML = `
            <p>> Rp ${priceConverter.format(dataInput)}</p>
        `
        }
    
    } else {
        if (dataInput.length === 0) {
            dataContainer.innerHTML = `
            <p>-</p>
        `
        } else {
            dataContainer.innerHTML = `
                <p>${dataInput}</p>
            `
        }
    }
}
// Menampilkan informasi filter END 


// Menampilkan data mobil hasil pencarian
let resultContainer = document.getElementById("search-result");

if (arrayMobil.length > 0) {
    for (let i = 0; i < arrayMobil.length; i++) {
        let objek = arrayMobil[i];
    
        let resultItem = document.createElement("div");
        
        let priceConverter = Intl.NumberFormat('ID');
        
        resultItem.innerHTML = `
            
            <img src=${objek.image} alt=${objek.name} />
            <h3>${objek.name}</h3>
            <h4>Rp ${priceConverter.format(objek.price)} / hari</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <button parent-index = ${i} class = "see-detail">Pilih Mobil</button>
        `
    
        resultContainer.appendChild(resultItem);
    }
} else {
    let resultItem = document.createElement("div");
    resultItem.innerHTML = `
        <p>Maaf, kriteria yang dicari tidak ditemukan.</p>
    `
    resultContainer.appendChild(resultItem);
}
// Menampilkan data mobil hasil pencarian END

// Edit filter dengan cara back to car search page
const editButton = document.getElementById("edit");

editButton.addEventListener("click", () => location.href = "car-search.html");
// Edit filter dengan cara back to car search page END


let buttonArray = document.querySelectorAll(".see-detail");
buttonArray.forEach( button => button.addEventListener("click", (e) => {
    let idButton = e.target.getAttribute("parent-index");
    let mobilPilihan = arrayMobil[idButton];
    
    localStorage.setItem('mobilPilihan', JSON.stringify(mobilPilihan))
    location.href = "car-detail.html"
}))





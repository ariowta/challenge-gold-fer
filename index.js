
// testimoni slide
let curSlide = 1;
let pergeseran = 0
const slide = document.querySelector(".testimoni__detail-container");
const nextSlide = document.querySelector(".button__right");
const leftSlide = document.querySelector(".button__left");

nextSlide.addEventListener("click", function () {
    curSlide++;
    if (curSlide > -1 && curSlide < 3) {
        pergeseran -= 34;
        slide.style.transform = `translateX(${pergeseran}%)`;
        slide.style.transition = `all 1s`
    } else if (curSlide === 3) {
        pergeseran += 68;
        slide.style.transform = `translateX(${pergeseran}%)`;
        curSlide = 0;
    }
})

leftSlide.addEventListener("click", function () {
    curSlide--;
    if (curSlide > -1 && curSlide < 3) {
        pergeseran += 34;
        slide.style.transform = `translateX(${pergeseran}%)`;
        slide.style.transition = `all 1s`
    } else if (curSlide === -1) {
        pergeseran -= 68;
        slide.style.transform = `translateX(${pergeseran}%)`;
        curSlide = 2;
    }
})
// End of testimoni slide